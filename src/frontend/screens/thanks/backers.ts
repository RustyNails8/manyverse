// SPDX-FileCopyrightText: 2023 The Manyverse Authors
//
// SPDX-License-Identifier: CC0-1.0

export default ['Dace', 'John Meurer', 'C Moid', 'DC Posch', 'Andrew Lewman'];
